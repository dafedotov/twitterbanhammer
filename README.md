# TwitterBanhammer

Set of scripts for blocking twitter accounts by name, key-words, bot-node.

* `update_lists.py` - updates the current state of the user's lists of followers, friends, mutes, and blocks.
* `ban_by_name.py` - tries to find users by part of their name and block them. 
* `ban_by_keywords.py` - tries to find users by used keywords and block them. 
* `ban_by_name.py` - block all followers of the account with followers more than 2000. 

After running scripts they update lists:
- if user is already in black list or followed it's skipped
- if user is in followers, it's muted
- else it's blocked
